export const getLaptops = async () => {
    const url = 'https://noroff-komputer-store-api.herokuapp.com/computers';
    let res = await fetch(url);
    let data = await res.json();
    return data;
}