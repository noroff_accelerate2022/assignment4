export const domElements = {
    // static values
    customerName: document.getElementById('customer-name'),
    customerBankBalance: document.getElementById('customer-balance'),
    customerLoanAmount: document.getElementById('customer-loan'),
    customerPayAmount: document.getElementById('customer-pay'),
    laptopPhoto: document.getElementById('laptop-photo'),
    laptopModel: document.getElementById('laptop-model-name'),
    laptopDescription: document.getElementById('laptop-model-description'),
    laptopPrice: document.getElementById('laptop-price-amount'),
    // buttons
    getLoanBtn: document.getElementById('get-loan-btn'),
    bankBtn: document.getElementById('bank-btn'),
    workBtn: document.getElementById('work-btn'),
    repayLoanBtn: document.getElementById('repay-loan-btn'),
    laptopBuyBtn: document.getElementById('laptop-buy-btn'),
    // containers
    laptopOverview: document.getElementById('laptop-box'),
    customerLoanOverview: document.getElementById('customer-loan-overview'),
    // laptop elements
    laptopDropdown: document.getElementById('laptops'),
    laptopInfo: document.getElementById('laptop-info'),
    laptopFeatures: document.getElementById('laptop-features'),
    laptopPhoto: document.getElementById('laptop-photo'),
}