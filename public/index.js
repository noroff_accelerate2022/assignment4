import { domElements } from "./util/domElements.js";
import { getLaptops } from "./util/laptops.js";

// data
const customer = {
    bankBalance: 200,
    loan: 0,
    pay: 0,
    salary: 100,
    name: "Walter White",
    laptops: [], // assuming that a customer can buy more than 1 laptop
}
const baseImageUrl = 'https://noroff-komputer-store-api.herokuapp.com/';
let laptops = await getLaptops();

// set initial state
domElements.customerName.innerText = customer.name;
domElements.customerBankBalance.innerText = customer.bankBalance;
domElements.customerPayAmount.innerText = customer.pay;

// update the laptop dropdown with the fetched data
laptops.forEach(laptop => {
    const laptopOption = document.createElement('option');
    laptopOption.text = laptop.title;
    domElements.laptopDropdown.add(laptopOption);
});

// display first laptop's features
appendLaptopFeatures(0);

// display laptop's detailed info
displayLaptop(laptops[0]);

// repay loan button and loan overview are invisible as there is no loan in the beginning
domElements.customerLoanOverview.style.display = 'none';
domElements.repayLoanBtn.style.display = 'none';

// bank button is disabled as there is no money that could be transferred
domElements.bankBtn.disabled = true;

// assign functions to interactive elements
domElements.getLoanBtn.addEventListener('click', takeLoan);
domElements.bankBtn.addEventListener('click', bank);
domElements.workBtn.addEventListener('click', work);
domElements.repayLoanBtn.addEventListener('click', repayLoan);
domElements.laptopDropdown.addEventListener('change', selectLaptop);
domElements.laptopBuyBtn.addEventListener('click', buyLaptop);

// *** FUNCTIONS ***

function takeLoan() {
    let loanAmount = Number(prompt('Enter the loan amount'));
    if (loanAmount) {    
        if (loanAmount > customer.bankBalance*2) {
                alert('You cannot take a loan that is more than a double of your bank balance!');
        } else {
            customer.loan = loanAmount;
            customer.bankBalance += loanAmount;

            // update the DOM
            domElements.customerLoanAmount.innerText = customer.loan;
            domElements.customerBankBalance.innerText = customer.bankBalance;
            domElements.customerLoanOverview.style.display = 'flex';
            domElements.repayLoanBtn.style.display = 'block';
            if (!customer.pay) {
                // cannot repay the loan
                domElements.repayLoanBtn.disabled=true;
            }

            // disable the button as the customer is only allowed to have 1 loan
            domElements.getLoanBtn.disabled = true;
        }
    }
}

function bank() {
    let loanPay = 0;
    if (customer.loan) {
        // deduct 10% of the pay to repay the loan
        loanPay = customer.pay*0.1;
        customer.loan -= loanPay;
        if (customer.loan <= 0) {
            handlePayedLoanRefund();
        }
        domElements.customerLoanAmount.innerText = customer.loan;
    }
    
    const transfer = customer.pay - loanPay;
    customer.bankBalance += transfer;
    customer.pay = 0;

    // update the DOM
    domElements.customerBankBalance.innerText = customer.bankBalance;
    domElements.customerPayAmount.innerText = customer.pay;
    // bank and repay buttons are not available now
    domElements.bankBtn.disabled = true;
    domElements.repayLoanBtn.disabled = true;
}

function work() {
    customer.pay += customer.salary;

    // update the DOM
    domElements.customerPayAmount.innerText = customer.pay;
    // bank and repay loan button are now enabled
    domElements.bankBtn.disabled = false;
    domElements.repayLoanBtn.disabled = false;
}

function repayLoan() {
    customer.loan -= customer.pay;
    customer.pay = 0;

    if (customer.loan <= 0) {
        handlePayedLoanRefund();
        domElements.customerBankBalance.innerText = customer.bankBalance;
    }

    // update the DOM
    domElements.customerPayAmount.innerText = customer.pay;
    domElements.customerLoanAmount.innerText = customer.loan;
    // disable bank button and repay button, as there is no money to be transferred
    domElements.bankBtn.disabled = true;
    domElements.repayLoanBtn.disabled = true;
}

function selectLaptop() {
    const selectedLaptopIndex = domElements.laptopDropdown.selectedIndex;
    const laptop = laptops[selectedLaptopIndex];

    // remove the old options
    domElements.laptopFeatures.innerHTML = '';
    
    // add new options
    appendLaptopFeatures(selectedLaptopIndex);

    // display laptop's detailed info
    displayLaptop(laptop);
    
    // check if the laptop is owned; if so, disable the buy button
    if (customer.laptops.includes(laptop)) {
        domElements.laptopBuyBtn.innerText = 'OWNED';
        domElements.laptopBuyBtn.disabled = true;
    } else {
        domElements.laptopBuyBtn.innerText = 'BUY NOW';
        domElements.laptopBuyBtn.disabled = false;
    }
}

function buyLaptop() {
    const selectedLaptopIndex = domElements.laptopDropdown.selectedIndex;
    const laptop = laptops[selectedLaptopIndex];

    // check if there are sufficient funds
    if (customer.bankBalance >= laptop.price) {
        customer.laptops.push(laptop);
        customer.bankBalance -= laptop.price;

        alert(`Congratulations! You have just bought the ${laptop.title}`);

        // update the DOM
        domElements.customerBankBalance.innerText = customer.bankBalance;
        // customer cannot buy the laptop anymore
        domElements.laptopBuyBtn.innerText = 'OWNED';
        domElements.laptopBuyBtn.disabled = true;
    } else {
        alert(`Unfortunately, you cannot buy the ${laptop.title}. You need ${laptop.price-customer.bankBalance}kr more.`);
    }
}


// *** UTILITY FUNCTIONS ***

function appendLaptopFeatures(laptopIndex) {
    const specs = laptops[laptopIndex].specs;

    specs.forEach(s => {
        const feature = document.createElement('li');
        feature.innerText = s;
        domElements.laptopFeatures.appendChild(feature);
    })
}

function displayLaptop(laptop) {
    domElements.laptopModel.innerText = laptop.title;
    domElements.laptopDescription.innerText = laptop.description;
    domElements.laptopPhoto.setAttribute('src', baseImageUrl+laptop.image);
    domElements.laptopPrice.innerText = laptop.price;
}

function handlePayedLoanRefund() {
    if (customer.loan < 0) {
        alert(`Your loan has been paid. The exceeding amount (${-(customer.loan)}kr) has been transferred to your bank balance.`);
        customer.bankBalance += -(customer.loan);
        customer.loan = 0;
    }

    // the customer can take the loan again
    domElements.getLoanBtn.disabled = false;
    // remove loan overview
    domElements.customerLoanOverview.style.display = 'none';
    // remove repay button
    domElements.repayLoanBtn.style.display = 'none';
}
