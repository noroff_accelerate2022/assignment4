# Assignment4

The application can be accessed [here](https://assignment4-komputer-store.herokuapp.com/).

This is a simple vanilla JavaScript application featuring a computer store with bank functionality.
The user can choose from range of laptop models and buy them, as long as there as sufficient funds 
in the user's account. The user can own more than one laptop. In order to afford a laptop, the user may also take a loan that cannot be higher than the doubled bank balance. In addition, only one loan can be taken at the time.

The user is working and can add `100kr` to their pay balance at each button click.

The user can repay their loan in two ways - either by using the `Repay loan` button, which will transfer all earned money to the loan, or by using the `Bank` button, which will automatically transfer 10% of the earned amount to the loan, while the rest will be transferred to the bank balance.
